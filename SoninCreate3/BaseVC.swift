//
//  BaseVC.swift
//  Exhibitor
//
//  Created by Subarna Santra on 13/12/2016.
//  Copyright © 2016 Sonin. All rights reserved.
//

import UIKit


class BaseVC: UIViewController, UITextFieldDelegate, UITextViewDelegate {

    var currentKeyboardRectangle: CGRect?
    
    var currentFieldInEditing : UIView?
    
    
    
    
    lazy var activityOverlay : ActivityOverlay = ActivityOverlay()
        
    
    var loaderVisible : Bool = false
    
    var isSelfVisible : Bool = false
    
   
    var dataRefreshedOnceAlready : Bool = false
   
    //var dataNeedsRefreshing : Bool = true
    
    @IBOutlet weak var targetScrollView: UIScrollView?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
     override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        isSelfVisible = false
        removeObserverForKeyboardNotification()
        
        super.viewWillDisappear(animated)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
        
    
    func showAlertMessage(messageHeader : String, messageString : String, afterCompletion: @escaping (_ hidden: Bool) -> ()) {
        
        if (self.view.window == nil){
            
            return
        }
        
        let alert = UIAlertController(title: messageHeader, message: messageString, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "OK", style: .default) { _ in
            
            afterCompletion(true)
        })
        
        self.present(alert, animated: true){}
    }
    
    
    func showConfirmMessage(messageString : String, cancelText: String = "Cancel", okText:String = "OK", messageHeader : String = "", afterCompletion: @escaping (_ hidden: Bool, _ accepted: Bool) -> ()) {
        
        if (self.view.window == nil){
            
            return
        }
        
        let alert = UIAlertController(title: messageHeader, message: messageString, preferredStyle: .alert)
        
        
        alert.addAction(UIAlertAction(title: cancelText, style: .destructive) { _ in
            
            afterCompletion(true, false)
        })
        
        alert.addAction(UIAlertAction(title: okText, style: .default) { _ in
            
            afterCompletion(true, true)
        })

        
        self.present(alert, animated: true){}
    }
    
    //MARK:  Keyboard Handler
    
    func addObserverForKeyboardNotification() {
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidShow(notification:)), name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidHide(notification:)), name: NSNotification.Name.UIKeyboardDidHide, object: nil)
    }
    
    
    func removeObserverForKeyboardNotification() {
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidShow, object: nil)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardDidHide, object: nil)
        
    }
    
    func keyboardDidShow(notification:NSNotification) {
        
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        
        self.currentKeyboardRectangle = keyboardRectangle
        
        //var pt = CGPointMake(0.0, 0.0)
        
        if (currentFieldInEditing != nil) {
            
            if (currentFieldInEditing!.isFirstResponder) {
                
                handleScrollingWhenKeyboardIsVisible(targetView: currentFieldInEditing!, keyboardRectangle: keyboardRectangle)
            }
        }
    }
    
    func keyboardWillHide(notification:NSNotification) {
        
        currentFieldInEditing = nil
        
        let pt = CGPoint(x: 0, y: 0)
        
        changeScrollViewContentOffset(offsetPt: pt, isViewObstructed: false)
    }
    
    
    func keyboardDidHide(notification:NSNotification) {
        
        handleScrollingWhenKeyboardIsHidden()
    }
    
    
    
    func keyboardWillShow(notification:NSNotification) {
        
        let userInfo:NSDictionary = notification.userInfo! as NSDictionary
        let keyboardFrame:NSValue = userInfo.value(forKey: UIKeyboardFrameEndUserInfoKey) as! NSValue
        let keyboardRectangle = keyboardFrame.cgRectValue
        
        self.currentKeyboardRectangle = keyboardRectangle
        
//        var pt = CGPoint(x: 0, y: 0)
//        
//        if (currentFieldInEditing != nil) {
//            
//            if (currentFieldInEditing!.isFirstResponder) {
//                
//                var rc = currentFieldInEditing!.bounds
//                rc = currentFieldInEditing!.convert(rc, to: view)
//                
//                let isViewObstructed = rc.intersects (keyboardRectangle)
//                
//                //if (isViewObstructed) {
//                    
//                    pt.y = (rc.origin.y + rc.size.height) - keyboardRectangle.origin.y
//                    
//                    changeScrollViewContentOffset(offsetPt: pt, isViewObstructed: isViewObstructed)
//                //}
//            }
//        }
        
    }
    
    
    
    func handleScrollingWhenKeyboardIsVisible(targetView : UIView, keyboardRectangle : CGRect) {
        
        self.currentKeyboardRectangle = keyboardRectangle
        
        if let currentScrollView = targetScrollView {
            
            var pt = CGPoint(x: 0.0, y: 0.0)
            
            pt = currentScrollView.contentOffset
            
            if (currentFieldInEditing != nil) {
                
                if (currentFieldInEditing!.isFirstResponder) {
                    
                    var viewRealPos = currentFieldInEditing!.bounds
                    viewRealPos = currentFieldInEditing!.convert(viewRealPos, to: view)
                    
                    var scrollViewRealPos = currentScrollView.bounds
                    
                    scrollViewRealPos = currentScrollView.convert(scrollViewRealPos, to: view)
                    
                    let isViewObstructed = viewRealPos.intersects (keyboardRectangle)
                    
                    pt.y = pt.y + (viewRealPos.origin.y + viewRealPos.size.height) - keyboardRectangle.origin.y - 50
                    
                    changeScrollViewContentOffset(offsetPt: pt, isViewObstructed: isViewObstructed)
                }
            }
        }
    }
    
    
    
    func handleScrollingWhenKeyboardIsHidden() {
        
        //if let currentScrollView = targetScrollView {
            
            //currentScrollView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
        //}
    }
    
    
    func changeScrollViewContentOffset(offsetPt: CGPoint, isViewObstructed: Bool) {
        
        if (isViewObstructed) {
            
            if let currentScrollView = targetScrollView, let keyboardRectangle = self.currentKeyboardRectangle {
            
                currentScrollView.contentInset = UIEdgeInsetsMake(0, 0, keyboardRectangle.size.height + 150, 0)
                
                currentScrollView.setContentOffset(offsetPt, animated: true)
            }
        }
    }
    
    
    func dismissKeyboard(){
        self.view.endEditing(true)
        
        
        if let editingField = currentFieldInEditing {
            
            editingField.resignFirstResponder()
        }
        
    }
    
    //MARK:  UITextFieldDelegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        currentFieldInEditing = textField
        
        return true
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        currentFieldInEditing = nil
        textField.resignFirstResponder()
        return true
    }
    
    //MARK:  UITextViewDelegate
    
    func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        
        currentFieldInEditing = nil
        textView.resignFirstResponder()
        return true
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        currentFieldInEditing = textView
        
        return true
        
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        
       
    }
}

