//
//  Calendar.swift
//  SoninCreate3
//
//  Created by James Coleman on 27/04/2017.
//  Copyright © 2017 Sonin. All rights reserved.
//

import Foundation
import EventKit
import MapKit

class Calendar {
    
    // http://stackoverflow.com/questions/28379603/how-to-add-an-event-in-the-device-calendar-using-swift
    /// Add an event to the device calendar.
    ///
    /// - Parameters:
    ///   - title: Main description of the event
    ///   - notes: A desciption if necessary
    ///   - startDate: When the event starts
    ///   - endDate: When the event ends
    ///   - mapItem: The location of the event
    ///   - completion: Completion handler. Returns (true, nil) if the event was succesfully added, else (false, error)
    static func addEventToCalendar(title: String, notes: String?, startDate: NSDate, endDate: NSDate, mapItem: MKMapItem = Location.soninMapItem, completion: ((_ success: Bool, _ error: NSError?) -> ())? = nil) {
        let eventStore = EKEventStore()
        
        eventStore.requestAccess(to: .event, completion: { (granted, error) in
            if (granted) && (error == nil) {
                let event = EKEvent(eventStore: eventStore)
                event.title = title
                event.startDate = startDate as Date
                event.endDate = endDate as Date
                event.notes = notes
                event.calendar = eventStore.defaultCalendarForNewEvents
                event.structuredLocation = EKStructuredLocation(mapItem: mapItem)
                do {
                    try eventStore.save(event, span: .thisEvent)
                } catch let e as NSError {
                    completion?(false, e)
                    return
                }
                completion?(true, nil)
            } else {
                completion?(false, error as NSError?)
            }
        })
    }
}
