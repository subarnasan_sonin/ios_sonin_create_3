//
//  Location.swift
//  SoninCreate3
//
//  Created by James Coleman on 27/04/2017.
//  Copyright © 2017 Sonin. All rights reserved.
//

import Foundation
import MapKit

class Location {
    static let soninCarParkLatitudeDecimal = 51.2365
    static let soninCarParkLongitudeDecimal = -0.2059
    
    static var soninCarParkCoordinateDecimalString: String {
        return "\(soninCarParkLatitudeDecimal), \(soninCarParkLongitudeDecimal)"
    }
    
    static var soninMapItem: MKMapItem {
        let coordinate = CLLocationCoordinate2DMake(soninCarParkLatitudeDecimal, soninCarParkLongitudeDecimal)
    
            let placemark = MKPlacemark(coordinate: coordinate)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = "Sonin HQ"
             return mapItem

    }
    
    /**
     Opens the location of the Sonin car park entrance in Apple Maps.
     Allows the user to navigate to it from there.
     */
    static func openSoninLocationInAppleMaps() {
        
        let latitude: CLLocationDegrees = soninCarParkLatitudeDecimal
        let longitude: CLLocationDegrees = soninCarParkLongitudeDecimal
        
        let regionDistance:CLLocationDistance = 1000
        let coordinate = CLLocationCoordinate2DMake(latitude, longitude)
        let regionSpan = MKCoordinateRegionMakeWithDistance(coordinate, regionDistance, regionDistance)
        let options = [
            MKLaunchOptionsMapCenterKey: NSValue(mkCoordinate: regionSpan.center),
            MKLaunchOptionsMapSpanKey: NSValue(mkCoordinateSpan: regionSpan.span)
        ]
        if #available(iOS 10.0, *) {
            let placemark = MKPlacemark(coordinate: coordinate)
            let mapItem = MKMapItem(placemark: placemark)
            mapItem.name = "Sonin"
            mapItem.openInMaps(launchOptions: options)
        } else {
            // Fallback on earlier versions
        }
       
    }
    
    // http://stackoverflow.com/questions/24670290/how-to-copy-text-to-clipboard-pasteboard-with-swift
    /// Copies the decimal coordinates of Sonin to the device pasteboard to allow them to be copied into any app.
    static func copySoninLocationDecimal() {
        UIPasteboard.general.string = soninCarParkCoordinateDecimalString
    }
}
