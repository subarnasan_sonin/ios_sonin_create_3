//
//  WebServices.swift
//  Exhibitor
//
//  Created by Saad Mirza on 10/11/2016.
//  Copyright © 2016 Sonin. All rights reserved.
//

import Foundation
import UIKit

//let CURRENT_USER_LOGGED_IN: String! = "SoninCrateUserLoggedIn"
//let LOGGED_USER_ID: String! = "SoninCrateLoggedUserId"
//let LOGGED_USER_INFO: String! = "SoninCrateLoggedUserInfo"


let LOGGED_USER_EMAIL : String = "LOGGED_USER_EMAIL"
let LOGGED_USER_ID : String = "LOGGED_USER_ID"
let UNIQUE_DEVICE_ID : String = "UNIQUE_DEVICE_ID"



class Network:NSObject, URLSessionDelegate, URLSessionTaskDelegate {
    
    var configuration =  URLSessionConfiguration.ephemeral
    
    static let sharedInstance = Network()
    
    
    var dataDefaults = UserDefaults.standard
    
    
    
    func getLoggedUserId() -> String {
        
        if let idUser =  UserDefaults.standard.object(forKey: LOGGED_USER_ID) as? String {
            return idUser
        }
        
        return ""
    }
    
    
    func getLoggedUserEmail() -> String {
        
        if let emailUser =  UserDefaults.standard.object(forKey: LOGGED_USER_EMAIL) as? String {
            return emailUser
        }
        
        return ""
    }
    
    func getDeviceIdentifier() -> String {
        
        if let idDevice =  UserDefaults.standard.object(forKey: UNIQUE_DEVICE_ID) as? String {
            return idDevice
        }
        
        return ""
    }
    
    func saveLoggedUserData(userId : String, userEmail: String, deviceId : String) {
        
        UserDefaults.standard.set(userId, forKey: LOGGED_USER_ID)
        
        UserDefaults.standard.set(userEmail, forKey: LOGGED_USER_EMAIL)
        
        UserDefaults.standard.set(deviceId, forKey: UNIQUE_DEVICE_ID)
        
        UserDefaults.standard.synchronize()
    }
    
    
    
    
    //    func getLoggedUserData() -> User? {
    //
    //        return self.currentCredentialManager.getLoggedUserData()
    //    }
    //
    //
    //    func saveAccessToken(userId:String , tokenData:Dictionary<String, AnyObject>) {
    //
    //        let _ = self.currentCredentialManager.saveValidAccessToken(userId: userId, tokenData: tokenData)
    //
    //    }
    //
    //
    //
    //    func saveLoggedUserData(userId : String, userData : Dictionary<String, AnyObject>) {
    //
    //        self.currentCredentialManager.saveLoggedUserData(userId: userId, userData: userData)
    //    }
    //
    //
    //    func logOutCurrentUser(onCompletion: @escaping (_ loggedOut: Bool,  _ message: String) -> ()) {
    //
    //        self.currentCredentialManager.cleanupCredentialData { (success, message) in
    //
    //            onCompletion(success, message)
    //        }
    //    }
    //
    //    func postLogoutUserNotification() {
    //
    //        self.currentCredentialManager.cleanupCredentialData { (success, message) in
    //        }
    //        NotificationCenter.default.post(name: NSNotification.Name(rawValue: NO_LOGGED_IN_USER_NOTIFICATION_KEY), object: nil)
    //    }
    
    
    
    
    func getDataFromJson(actionName: String, blnWebHook: Bool, withAuth: Bool = true, parameters:[String:String]?, completion: @escaping (_ success: Bool, _ dict: NSDictionary, _ message: String) -> Void) {
        
        var urlString = BASE_URL+actionName;
        
        var currentParameters :[String:String] = [:]
        
        if let givenParameters =  parameters {
            
            currentParameters = givenParameters
        }
        
        if blnWebHook {
            
            urlString = "https://hooks.slack.com/services/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"
            
            let type =  parameters?["notification_type"]
            
            currentParameters["username"] = "SoninBot"
            currentParameters["icon_url"] =  "create-iot-ios.sonin-staging.agency/img/sonin.jpg"
            currentParameters["channel"] = "ios_create-day_team"
            
            if type == "PARKING" {
                
                currentParameters["text"] = "Client has reached their parking slot"
            }
            else if type ==  "MAIN_DOOR"{
                currentParameters["text"] = "Client has reached the main door - Buzz them in! <@lauren>"
            }
            else if type == "BUZZ"{
                
                currentParameters["text"] = "Client has reached the main door - Please let them in! <@lauren>"
            }
            else{
                
                 let bev = type?.components(separatedBy: "-").last
                
                currentParameters["text"] = "Client has requested \(bev! )!"
            }
            
        }
        
        
        let headers = [
            "content-type": "application/json"
        ]
        
        
        
        print(urlString)
        //currentParameters["strAction"] = actionName
        
        if (withAuth) {
            
            currentParameters["user_id"] = self.getLoggedUserId()
        }
        
        DispatchQueue.global(qos: .background).async {
            
            Network.sharedInstance.downloadDataFromWebServices(baseUrl: urlString, parameters: currentParameters, headers: headers, completions: { ( success: Bool, dict: NSDictionary ) in
                print("dict is listing\(dict)")
                
                
                var receivedMessage : String = ""
                
                if let message = dict["message"]  as? String {
                    
                    receivedMessage = message
                    
                    
                    //                    if  message.range(of: "There is no logged in user") != nil {
                    //
                    //                        //callBackBlock (error: true, resultDict: nil , messageString: strMessage)
                    //
                    //                        if let strongSelf = self {
                    //
                    //                            strongSelf.postLogoutUserNotification()
                    //
                    //                            return
                    //                        }
                    //
                    //                    } else if  message.range(of: "The logged in user does not exist") != nil {
                    //
                    //                        //callBackBlock (error: true, resultDict: nil , messageString: strMessage)
                    //
                    //                        if let strongSelf = self {
                    //
                    //                            strongSelf.postLogoutUserNotification()
                    //
                    //                            return
                    //                        }
                    //
                    //                    }
                }
                
                var isSuccessful : Bool = success
                
                if let receivedSuccess = dict["success"]  as? Bool {
                    
                    isSuccessful = receivedSuccess
                    
                } else if let receivedSuccess = dict["success"]  as? Int {
                    
                    if (receivedSuccess == 1) {
                        
                        isSuccessful = true
                        
                    } else {
                        
                        isSuccessful = false
                    }
                }
                
                DispatchQueue.main.async {
                    
                    completion(isSuccessful, dict, receivedMessage)
                }
            })
        }
    }
    
    
    func downloadDataFromWebServices(baseUrl:String, parameters:[String:String], headers: [ String:String], completions: @escaping (_ success: Bool, _ result: NSDictionary) -> Void) {
        
        
        do {
            
            let postData =  try JSONSerialization.data(withJSONObject: parameters, options :[])
            
            
            
            var request = URLRequest(url: URL(string:baseUrl)! as URL,
                                     //cachePolicy: .useProtocolCachePolicy,
                timeoutInterval: 60.0)
            request.httpMethod = "POST"
            request.allHTTPHeaderFields = headers
            request.httpBody = postData
            
            
            //let session = URLSession.shared
            //let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            
            
            let dataTask = self.session().dataTask(with: request, completionHandler: { (data, response, error) -> Void in
                
                
                //if let strongSelf = self {
                
                if (error != nil) {
                    
                    completions(false, ["strMessage": error!.localizedDescription])
                    
                } else {
                    
                    
                    do{
                        let jsonData = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                        
                        
                        print("printing from webservice \(jsonData)")
                        
                        completions(true, jsonData)
                        
                    } catch{
                        
                        print("json error \(error.localizedDescription)")
                        //strongSelf.postLogoutUserNotification()
                        
                        completions(false, ["strMessage": error.localizedDescription])
                        
                        return
                    }
                }
                //}
            })
            
            dataTask.resume()
            
            
        }catch{
            print("Json serialization failed: \(error.localizedDescription)")
            //self.postLogoutUserNotification()
            completions(false, ["strMessage": error.localizedDescription])
            return
        }
        
        return
    }
    
    
    func session() -> URLSession{
        
        let session = URLSession(configuration: configuration,
                                 delegate: self,
                                 delegateQueue:OperationQueue.main)
        
        return session
    }
    
}
