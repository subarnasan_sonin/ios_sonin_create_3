//
//  WebServices.swift
//  Exhibitor
//
//  Created by Saad Mirza on 10/11/2016.
//  Copyright © 2016 Sonin. All rights reserved.
//

import Foundation
import UIKit

enum NotificationType : String {
    
    case PARKING = "PARKING"
    case MAIN_DOOR = "MAIN_DOOR"
}

class WebServices:NSObject, URLSessionDelegate, URLSessionTaskDelegate {
    
    static let sharedInstance = WebServices()
    
    
    
    //get client meeting info
    
    func getMeetingInfo(completion: @escaping (_ success: Bool, _ dict: NSDictionary, _ meetingTime: String, _ message: String) -> Void) {
        
        
        let parameters = ["": ""]
        
        Network.sharedInstance.getDataFromJson(actionName: "/client/meetings", blnWebHook:false ,  withAuth: true, parameters: parameters) { (success, dict, message) in
            
//            if (success) {
//                
//                
//                if let receivedData = dict.object(forKey: "data") as? NSDictionary {
//                    
//                    if let userId = receivedData.object(forKey: "id") as? String {
//                        
//                       
//                    }
//                }
//                
//            }
            
            completion(success, dict, "", message)
        }
    }
    
    //send client drink information
    
    func sendClientDrinkInfo(drinkType: String, completion: @escaping (_ success: Bool, _ dict: NSDictionary, _ message: String) -> Void) {
        
        
        let parameters = ["drink": drinkType]
        
        Network.sharedInstance.getDataFromJson(actionName: "/client/meeting/pick-drink", blnWebHook:false, withAuth: true, parameters: parameters) { (success, dict, message) in
                      
            if (success) {
                
                self.sendNotification(notificationType: "DRINK_REQUEST-\(drinkType)",blnWebHook: true, completion: { (success, dict, message1) in
                    
                })
            }
            
            completion(success, dict, message)
        }
    }
    
    
    //send client drink information
    
    func sendBuzzer(buzzerAt: String = "", completion: @escaping (_ success: Bool, _ dict: NSDictionary, _ message: String) -> Void) {
        
        
        let parameters = ["notification_type": buzzerAt]
        
        Network.sharedInstance.getDataFromJson(actionName: "/client/arrival-notification", blnWebHook:true, withAuth: true, parameters: parameters) { (success, dict, message) in
            
            //            if (success) {
            //
            //
            //                if let receivedData = dict.object(forKey: "data") as? NSDictionary {
            //
            //                    if let userId = receivedData.object(forKey: "id") as? String {
            //
            //
            //                    }
            //                }
            //                
            //            }
            
            completion(success, dict, message)
        }
    }
    
    
    func sendClientDetails(userEmail: String, uniqueId: String, completion: @escaping (_ success: Bool, _ dict: NSDictionary, _ message: String) -> Void) {
        
        
        let parameters = ["email": userEmail, "unique_id": uniqueId]
        
        print(parameters)
        
        Network.sharedInstance.getDataFromJson(actionName: "client/create", blnWebHook:false, withAuth: false, parameters: parameters) { (success, dict, message) in
            
            if (success) {
                
                
                if let receivedData = dict.object(forKey: "data") as? NSDictionary {
                    
                    if let userId = receivedData.object(forKey: "id") as? String {
                        
                        Network.sharedInstance.saveLoggedUserData(userId : userId, userEmail: userEmail, deviceId : uniqueId)
                        
                    } else if let userId = receivedData.object(forKey: "id") as? Int {
                        
                        Network.sharedInstance.saveLoggedUserData(userId : String(userId), userEmail: userEmail, deviceId : uniqueId)
                    }
                }
                
            }
            
            completion(success, dict, message)
        }
    }
    
    
    //send different kind of notifications
    
    func sendNotification(notificationType: String, blnWebHook:Bool, completion: @escaping (_ success: Bool, _ dict: NSDictionary, _ message: String) -> Void) {
        
        
        let parameters = ["notification_type": notificationType]
        
        print(parameters)
        
        Network.sharedInstance.getDataFromJson(actionName: "client/arrival-notification", blnWebHook:blnWebHook, withAuth: true, parameters: parameters) { (success, dict, message) in
            
            completion(success, dict, message)
        }
    }
    
    
}
