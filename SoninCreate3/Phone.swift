//
//  Phone.swift
//  SoninCreate3
//
//  Created by James Coleman on 27/04/2017.
//  Copyright © 2017 Sonin. All rights reserved.
//

import UIKit

class Phone {
    /**
     Makes a call to a number. Defaults to Sonin's number (01737457788)
    */
    static func call(number: String = "01737457788") {
        if let url = URL(string: "telprompt://\(number)") {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(url)
            } else {
                // Fallback on earlier versions
            }
        }
    }
}
