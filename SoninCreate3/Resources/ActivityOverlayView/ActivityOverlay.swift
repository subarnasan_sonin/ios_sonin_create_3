//
//  ActivityOverlay.swift
//  Ashcourt
//
//  Created by Subarna Santra on 06/06/2016.
//  Copyright © 2016 Big Orange Software. All rights reserved.
//

import UIKit

class ActivityOverlay: NSObject {    
    
    var overlayView : ActivityOverlayView?

    var backgroundColor : UIColor = UIColor.black
    
    var foregroundColor : UIColor = UIColor.white
    
    var textColor : UIColor = UIColor.white
    
    
    
    
    func createOverlay(onTopOf targetView : UIView, withBackground backgroundType : String = "LIGHT") {
        
        if (overlayView == nil) {
            
            overlayView = loadOverlayViewFromNib()
        }
        
        if (overlayView != nil) {
            
            
            if (backgroundType == "DARK") {
                
                backgroundColor = UIColor.black
                
                foregroundColor = UIColor.white
                
                textColor = UIColor.white
                
                overlayView?.backgroundOpacity = 0.7
                
            } else if (backgroundType == "LIGHTDARK") {
                
                backgroundColor = UIColor.black
                
                foregroundColor = UIColor.white
                
                textColor = UIColor.white
                
                overlayView?.backgroundOpacity = 0.2
                
                
            } else if (backgroundType == "LIGHT") {
                
                backgroundColor = UIColor.white
                
                foregroundColor = UIColor.black
                
                textColor = UIColor.gray
                
            } else if (backgroundType == "CLEAR") {
                
                backgroundColor = UIColor.clear
                
                foregroundColor = UIColor.black
                
                textColor = UIColor.gray
                
                overlayView?.backgroundOpacity = 0
                
            }
            
            overlayView?.overlayColor = backgroundColor
            
            overlayView?.foregroundColor = foregroundColor
            
            overlayView?.textColor = textColor
                       
            targetView.addSubview(overlayView!)
            
            targetView.sendSubview(toBack: overlayView!)
            
            overlayView!.isHidden = true
        }
    }
    
    

    func setOverlay(showTopView : Bool = true, targetView : UIView? = nil) -> Bool {


        if (overlayView != nil) {
            
            if let parentView = overlayView!.superview {
                
                parentView.layoutIfNeeded()
                parentView.setNeedsLayout()
                
                
                if (targetView != nil) {
                    
                    overlayView!.translatesAutoresizingMaskIntoConstraints = false
                                        
                    
                    parentView.addConstraints([
                        NSLayoutConstraint(item: overlayView!, attribute: .leading, relatedBy: .equal, toItem: targetView!, attribute: .leading, multiplier: 1, constant: 0),
                        NSLayoutConstraint(item: overlayView!, attribute: .trailing, relatedBy: .equal, toItem: targetView!, attribute: .trailing, multiplier: 1, constant: 0),
                        NSLayoutConstraint(item: overlayView!, attribute: .top, relatedBy: .equal, toItem: targetView!, attribute: .top, multiplier: 1, constant: 0),
                        NSLayoutConstraint(item: overlayView!, attribute: .bottom, relatedBy: .equal, toItem: targetView!, attribute: .bottom, multiplier: 1, constant: 0)
                        ])
                    

                    
                } else {
                    
                    var overlayFrame = parentView.bounds
                    
                    if (showTopView == true) {
                        
                        
                        //let topViewHeight = overlayFrame.size.height * 0.09
                        
                        let topViewHeight:CGFloat = 70
                        
                        //let topViewHeight:CGFloat = 0
                        
                        overlayFrame.origin.y = overlayFrame.origin.y + topViewHeight
                        
                        
                        overlayView!.moveLoaderX = -topViewHeight

                    }
                    
                    overlayView!.frame = overlayFrame
                    
                }
                
                parentView.bringSubview(toFront: overlayView!)
                
                return true

            }
        }

        return false
    }
    
    
    func hideOverlayAfterShowing(withMessage : String = "", delayTime : Double = 0.5, showTopView : Bool = true, distanceFromCenterX : CGFloat = 0, targetView : UIView? = nil) {
        
        if (setOverlay(showTopView : showTopView, targetView: targetView)) {
            
            if (showTopView == true) {
                
                overlayView?.show(withMessage: withMessage, distanceFromCenter: distanceFromCenterX)
                
            } else {
                
                overlayView?.show(withMessage: withMessage)
            }
            
            
            Timer.scheduledTimer(timeInterval: delayTime, target: self, selector: #selector(self.hideOverlay), userInfo: nil, repeats: false)
            
            return
        }
    }
    
    func showOverlay(withMessage : String = "", showTopView : Bool = true, showLoader : Bool = true, distanceFromCenterY : CGFloat = 0, targetView : UIView? = nil) {
        
        if (setOverlay(showTopView : showTopView, targetView: targetView)) {
            
            if (showTopView == true) {
                
                // overlayView?.show(withMessage, distanceFromCenter: distanceFromCenterY)
                overlayView?.show(withMessage: "", distanceFromCenter: distanceFromCenterY, showLoader : showLoader)
                
            } else {
                
                // overlayView?.show(withMessage)
                overlayView?.show(withMessage: "", showLoader : showLoader)
            }
        }
    }
    
    func showActivityOk(pulse : Bool = true, pulseDuration : Double = 0.6, repeatCount : Float = 1, withMessage : String = "", afterCompletion: ()->()) {
        
        overlayView?.showActivityOk(pulse : pulse, pulseDuration : pulseDuration, repeatCount: repeatCount, withMessage: withMessage, afterCompletion : afterCompletion)
    }
    
    func hideOverlay() {
        
        overlayView?.hide()
    }
    
    
    func  isHidden() -> Bool{
        
        if let targetView = self.overlayView {
            
            return targetView.isHidden
        }
        return true
    }
    
    func loadOverlayViewFromNib() -> ActivityOverlayView {
        
        let bundle = Bundle(for: type(of: self))
        
        let nib = UINib(nibName: "ActivityOverlayView", bundle: bundle)
        
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! ActivityOverlayView
        
        return view
    }


}
