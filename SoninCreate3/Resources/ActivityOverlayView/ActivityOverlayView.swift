//
//  ActivityOverlayView.swift
//  Ashcourt
//
//  Created by Subarna Santra on 06/06/2016.
//  Copyright © 2016 Big Orange Software. All rights reserved.
//

import UIKit

let BOSStrokeAnimationKey: String! = "BOSAnimationStroke"
let BOSRotationAnimationKey: String! = "BOSAnimationRoration"

class ActivityOverlayView: UIView {
    
    
    @IBOutlet weak var overlayContainerView: UIView!
    
    var overlayColor : UIColor = UIColor.black
    
    var foregroundColor : UIColor = UIColor.white
    
    var textColor : UIColor = UIColor.white
    
    var backgroundOpacity : CGFloat = 0.5
    
    
    @IBOutlet weak var loaderContainerView: UIView!
    
    @IBOutlet weak var loaderMessageLabel: UILabel!
    
    
    @IBOutlet weak var activityOkImageView: UIImageView!
    
    @IBOutlet weak var loaderViewXConstraint: NSLayoutConstraint!
    
     var moveLoaderX : CGFloat = 0.0
    
    
    var loaderConfigured : Bool = false
    
    
    
    let circlePathLayer = CAShapeLayer()
    var rotating : Bool = false
    var visible : Bool = false
    
    var circleRadius: CGFloat = 15.0
    
    var progress: CGFloat {
        get {
            return circlePathLayer.strokeEnd
        }
        set {
            if (newValue > 1) {
                circlePathLayer.strokeEnd = 1
            } else if (newValue < 0) {
                circlePathLayer.strokeEnd = 0
            } else {
                circlePathLayer.strokeEnd = newValue
            }
        }
    }
    
    func adjustPosition(distanceFromCenter : CGFloat = 0) {
        
        loaderViewXConstraint.constant = moveLoaderX + distanceFromCenter
        
        
        self.layoutIfNeeded()
        self.layoutSubviews()
    }
    
    
    func adjustColor() {
        
        self.backgroundColor = overlayColor.withAlphaComponent(backgroundOpacity)
        
        self.loaderMessageLabel.textColor = textColor
    }
    

    func configure(distanceFromCenter : CGFloat = 0) {
        
        if (loaderConfigured == false) {
            
            
            self.adjustPosition(distanceFromCenter: distanceFromCenter)
            
            self.adjustColor()
            
            
            circlePathLayer.frame = loaderContainerView.frame
            
            circlePathLayer.lineWidth = 2
            
            circlePathLayer.fillColor = UIColor.clear.cgColor
            circlePathLayer.strokeColor = foregroundColor.cgColor
            
            
            
            self.layer.addSublayer(circlePathLayer)
            
            
            
            
            
            let halfWidth = loaderContainerView.bounds.size.width/2
            
            if (halfWidth < circleRadius) {
                
                circleRadius = halfWidth
            }
            
            
            
            circlePathLayer.frame = loaderContainerView.frame
            
            circlePathLayer.path = circlePath().cgPath
            
          

            
            
            //layer.mask = circlePathLayer
            
           // self.clipsToBounds = true
            
            //layer.backgroundColor = UIColor.whiteColor().CGColor
            
           // self.layer.backgroundColor = UIColor.whiteColor()
            
            progress = 0
            
            loaderConfigured = true
        }
        
    }
    
    
    func circleFrame() -> CGRect {
        var circleFrame = CGRect(x: 0, y: 0, width: 2*circleRadius, height: 2*circleRadius)
        
        circleFrame.origin.x = circlePathLayer.bounds.midX - circleFrame.midX
        
        circleFrame.origin.y = circlePathLayer.bounds.midY - circleFrame.midY
        return circleFrame
    }
    
    func circlePath() -> UIBezierPath {
        return UIBezierPath(ovalIn: circleFrame())
    }
    
    /*override func layoutSubviews() {
        super.layoutSubviews()
        
        
    }*/
    
    func showLoader() {
        
        if (visible == false) {
            
            self.circlePathLayer.isHidden = false
            
            progress = 0.95
          
            visible = true
            startRotation()
        }
        
    }
    
    func hideLoader() {
        
        if (visible == true) {
          
            
            visible = false
            
            self.circlePathLayer.isHidden = true
            
            stopRotation()
        }
        
    }
    
    func stopRotation(){
        
        if (rotating == false) {
            return
        }
        
        circlePathLayer.removeAnimation(forKey: BOSRotationAnimationKey)
        circlePathLayer.removeAnimation(forKey: BOSRotationAnimationKey)
        
        rotating = false
    }

    
    func startRotation(){
        
        if (rotating == true) {
            return
        }
        
        rotating = true
        
        let duration = 1.0
        
        let rotationAnimation = CABasicAnimation()
        rotationAnimation.keyPath = "transform.rotation"
        
        rotationAnimation.duration = duration/0.75
        
        rotationAnimation.fromValue = 0.0
        
        rotationAnimation.toValue = 2 * M_PI
        
        rotationAnimation.repeatCount = Float.infinity
        
        rotationAnimation.isRemovedOnCompletion =  false
        circlePathLayer.add(rotationAnimation, forKey: BOSRotationAnimationKey)
       
        
    }
    
    
    func showActivityOk(pulse : Bool = true, pulseDuration : Double = 0.6, repeatCount : Float = 4, withMessage : String = "", distanceFromCenterX : CGFloat = 0, afterCompletion: ()->()) {
        
        
        self.hideLoader()
        
        self.adjustPosition(distanceFromCenter: distanceFromCenterX)
        
        self.adjustColor()
        
        self.loaderMessageLabel.text = withMessage
        
        self.activityOkImageView.isHidden = false
        
        if (pulse == true) {
            
            //self.activityOkImageView.pulse(pulseDuration, repeatCount: repeatCount, afterCompletion : afterCompletion)
        }
        
        
        self.isHidden = false
    }

    
    func show(withMessage : String = "", distanceFromCenter : CGFloat = 0, showLoader : Bool = true) {
        
       
        self.configure(distanceFromCenter: distanceFromCenter)
        
        self.loaderMessageLabel.text = withMessage
        
        self.activityOkImageView.isHidden = true
        
        
        if (showLoader == true) {
            
            self.showLoader()
            
        } else {
            
            self.hideLoader()
        }
        
        
        
        self.isHidden = false
    
    }
    
    func hide() {
        self.isHidden = true
        
        self.loaderMessageLabel.text = ""
        
        self.hideLoader()
        
    }


}
