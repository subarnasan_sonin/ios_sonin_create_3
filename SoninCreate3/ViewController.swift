//
//  ViewController.swift
//  SoninCreate3
//
//  Created by Sonin on 27/04/2017.
//  Copyright © 2017 Sonin. All rights reserved.
//

import UIKit
import MessageUI
import UserNotifications

@available(iOS 10.0, *)
class ViewController: BaseVC, MFMailComposeViewControllerDelegate, ESTBeaconManagerDelegate, UNUserNotificationCenterDelegate {
    
    let beaconManager = ESTBeaconManager()
    let appDelegate : AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    let parkingRegion      = CLBeaconRegion(proximityUUID: UUID(uuidString: "A3B7E9F9-F3AE-423C-B9A1-13ED4441E7AD")!, major: 29908, minor: 38912, identifier: "Parking")
    let mainDoorRegion     = CLBeaconRegion(proximityUUID: UUID(uuidString: "A7D143F7-1AD9-4B10-9A8C-5F747CF2EA57")!, major: 61293, minor: 52679, identifier: "MainDoor")
    let meetingRoomRegion  = CLBeaconRegion(proximityUUID: UUID(uuidString: "B9407F30-F5F8-466E-AFF9-25556B57FE6D")!, major: 1378, minor: 61314, identifier: "MeetingRoom")
    
    let center = UNUserNotificationCenter.current()
    
    
    @IBOutlet weak var mapView: UIView!
    
    @IBOutlet weak var meetingTimeLabel: UILabel!
    
    
    @IBOutlet weak var directionButton: UIButton!
    
    @IBOutlet weak var meetingInfoView: UIView!
    
    
    @IBOutlet weak var companyView: UIView!
    
    @IBOutlet weak var phoneButton: UIButton!
    
    @IBOutlet weak var emailButton: UIButton!
    
    
    @IBOutlet weak var drinkView: UIView!
    
    @IBOutlet weak var waterButton: UIButton!
    
    
    @IBOutlet weak var coffeeButton: UIButton!
    
    
    @IBOutlet weak var buzzerView: UIView!
    
    @IBOutlet weak var liftCodeLabel: UILabel!
    
    @IBOutlet weak var wifiPassLabel: UILabel!
    
    
    @IBOutlet weak var seeYouAgainLabel: UILabel!
    
    @IBOutlet weak var teaButton: UIButton!
    
    
    @IBOutlet weak var buzzerButton: UIButton!
    
    //MARK: - Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.setupView()
        self.beaconManager.delegate = self
        self.beaconManager.requestAlwaysAuthorization()
        
        self.beaconManager.startMonitoring(for: parkingRegion)
        self.beaconManager.startMonitoring(for: mainDoorRegion)
        self.beaconManager.startMonitoring(for: meetingRoomRegion)
        
        self.beaconManager.startRangingBeacons(in: meetingRoomRegion)
        
        
        // Do any additional setup after loading the view, typically from a nib.
        //        Location.openSoninLocationInAppleMaps()
        //        Location.copySoninLocationDegrees()
        //
        //        Calendar.addEventToCalendar(title: "Test Event", notes: "This is a test event", startDate: NSDate().addingTimeInterval(3600), endDate: NSDate().addingTimeInterval(7200))
        //
        //   sendEmailButtonTapped(sender: self)
        
        // Phone.call()
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - INITIAL SETUP
    
    func setupView() {
        
        self.activityOverlay.createOverlay(onTopOf: self.view)
    }
    
    //MARK: - HANDLE NOTIFICATIONS
    
    
    func showLiftCode(code:String) {
        
        self.liftCodeLabel.text = code
    }
    
    func showWifiInfo(name:String, password:String) {
        
        self.wifiPassLabel.text = "Password: "+password
    }
    
    func showSeeyouSoonLabel() {
        
        self.seeYouAgainLabel.isHidden = false
    }
    
    
    
    //MARK: - DIRECTION ACTION
    
    @IBAction func directionAction(_ sender: Any) {
        Location.openSoninLocationInAppleMaps()
        
    }
    
    
    //MAKR: - send client drink preference
    
    
    @IBAction func teaAction(_ sender: Any) {
        
        self.sendDrinkInfo(drinkType: "Tea")
    }
    
    @IBAction func coffeeAction(_ sender: Any) {
        
        self.sendDrinkInfo(drinkType: "Coffee")
    }
    
    @IBAction func waterAction(_ sender: Any) {
        
        self.sendDrinkInfo(drinkType: "Water")
    }
    
    func sendDrinkInfo(drinkType: String) {
        
        self.activityOverlay.showOverlay()
        
        WebServices.sharedInstance.sendClientDrinkInfo(drinkType: drinkType, completion: { [weak self] (success, dict, message) in
            
            if let strongSelf = self {
                
                strongSelf.activityOverlay.hideOverlay()
                
                if (success) {
                    
                    switch drinkType {
                        
                    case "Tea":
                        
                        strongSelf.teaButton.isEnabled = false
                        
                        strongSelf.coffeeButton.isHidden = true
                        
                        strongSelf.waterButton.isHidden = true
                        
                        break
                        
                    case "Coffee":
                        
                        strongSelf.teaButton.isHidden = true
                        
                        strongSelf.coffeeButton.isEnabled = false
                        
                        strongSelf.waterButton.isHidden = true
                        
                        break
                        
                    case "Water":
                        
                        strongSelf.teaButton.isHidden = true
                        
                        strongSelf.coffeeButton.isHidden = true
                        
                        strongSelf.waterButton.isEnabled = false
                        
                        break
                    default:
                        break
                    }
                    
                } else {
                    
                    strongSelf.showAlertMessage(messageHeader: "Sorry!!", messageString: message, afterCompletion: { (_) in
                        
                    })
                }
            }else{
                self?.activityOverlay.hideOverlay()
            }
            
        })
    }
    
    
    //MARK: BUZZER ACTIONS
    
    
    @IBAction func buzzerAction(_ sender: Any) {
        
        self.buzzerButton.isEnabled = false
        
        self.activityOverlay.showOverlay()
        
        WebServices.sharedInstance.sendBuzzer(buzzerAt: "BUZZ", completion: { [weak self] (success, dict, message) in
            
            self?.activityOverlay.hideOverlay()
            
            if let strongSelf = self {
                
                strongSelf.buzzerButton.isEnabled = true
                
                if (success) {
                    
                    strongSelf.showAlertMessage(messageHeader: "", messageString: "Welcome!! Some one will open the door soon.", afterCompletion: { (_) in
                        
                    })
                    strongSelf.activityOverlay.hideOverlay()
                } else {
                    
                    // strongSelf.showAlertMessage(messageHeader: "Sorry!!!", messageString: "Something went wrong...Could you please try the front door buzzer.", afterCompletion: { (_) in
                    
                    // })
                }
            }
            
        })
    }
    
    
    //MARK: - GET MEETING DETAILS
    
    func getMeetingDetails() {
        
        
        self.buzzerButton.isEnabled = false
        
        self.activityOverlay.showOverlay()
        
        WebServices.sharedInstance.getMeetingInfo(completion: { [weak self] (success, dict, time, message) in
            
            if let strongSelf = self {
                
                strongSelf.meetingTimeLabel.text = time
                
                strongSelf.activityOverlay.hideOverlay()
                
            }
            
        })
    }
    
    
    //MAKR: - send client details for the first time
    
    
    
    func showDetails() {
        
        
    }
    
    //MARK: - IBACTIONS
    
    @IBAction func phoneAction(_ sender: Any) {
        
        self.makePhoneCall(contactDetails: "01737457788")
    }
    
    @IBAction func emailAction(_ sender: Any) {
        
        self.showEmailComposer(toMail: "test@testmail.com")
    }
    
    
    //MARK: - PHONE CALL
    
    func makePhoneCall(contactDetails: String) {
        
        if (!contactDetails.isEmpty) {
            
            if let callUrl : URL = URL(string: "tel://\(contactDetails)") {
                
                let application : UIApplication = UIApplication.shared
                
                if (application.canOpenURL(callUrl)) {
                    application.open(callUrl, options: [:], completionHandler: { (bln) in
                        
                    })
                }
            }
        }
    }
    
    //MARK: - EMAIL COMPOSER
    
    func showEmailComposer(toMail contactEmail: String) {
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            
            mail.setToRecipients([contactEmail])
            
            present(mail, animated: true)
        } else {
            
            self.showAlertMessage(messageHeader: "Sorry!!", messageString: "Your device could not send e-mail.  Please check e-mail configuration and try again.", afterCompletion: { (_) in
                
            })
        }
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    
    // MARK: - Monitoring Delegate
    func beaconManager(_ manager: Any, didEnter region: CLBeaconRegion) {
        
        DispatchQueue.main.async {
            
            var body: String? = ""
            
            let content = UNMutableNotificationContent()
            
            content.sound = UNNotificationSound.default()
            
            let trigger = UNTimeIntervalNotificationTrigger(
                timeInterval: 1.0,
                repeats: false)
            
            
            if region == self.parkingRegion{
                
                // DispatchQueue.once(token: "maindoor", block: { (any) in
                
                WebServices.sharedInstance.sendNotification(notificationType: "PARKING",blnWebHook: true, completion: { (blnResult, dict, value) in
                    
                    
                })
                
                let identifier = "parkingIden"
                let content = UNMutableNotificationContent()
                content.title = "Welcome"
                content.sound = UNNotificationSound.default()
                content.body = "Hi \(self.appDelegate.clientEmail), please make your way to the main entrance"
                content.setValue(true, forKeyPath: "shouldAlwaysAlertWhileAppIsForeground")
                let request = UNNotificationRequest(identifier: identifier,
                                                    content: content, trigger: trigger)
                
                self.center.add(request, withCompletionHandler: { (error) in
                    if error != nil {
                        print("Sonething wrong")
                    }
                })
                //})
            }
            else if region == self.mainDoorRegion {
                
                WebServices.sharedInstance.sendNotification(notificationType: "MAIN_DOOR",blnWebHook: true, completion: { (blnResult, dict, value) in
                    
                    
                })
                
                let identifier = "maindoorIden"
                content.title = "Welcome"
                body = "Please use app buzzer to enter the building. The lift code is \(self.appDelegate.LIFT_CODE)"
                content.body = body!
                content.setValue(true, forKeyPath: "shouldAlwaysAlertWhileAppIsForeground")
                let request = UNNotificationRequest(identifier: identifier,
                                                    content: content, trigger: trigger)
                //  DispatchQueue.once(token: "maindoor", block: { (any) in
                
                self.center.add(request, withCompletionHandler: { (error) in
                    if error != nil {
                        print("Sonething wrong")
                    }
                })
                
                // })
                
            }
            else if region == self.meetingRoomRegion{
                
                let identifier = "meetingIden"
                content.title = "Welcome"
                body = "Welcome to Sonin, beverages are on their way! Your wifi SSID is \(self.appDelegate.WIFI_SSID) and password is \(self.appDelegate.WIFI_PASSWORD)"
                content.body = body!
                content.setValue(true, forKeyPath: "shouldAlwaysAlertWhileAppIsForeground")
                let request = UNNotificationRequest(identifier: identifier,
                                                    content: content, trigger: trigger)
                //   DispatchQueue.once(token: "maindoor", block: { (any) in
                self.center.add(request, withCompletionHandler: { (error) in
                    if error != nil {
                        print("Sonething wrong")
                    }
                })
                // })
            }
        }
    }
    
   
    
 
    
    // MARK: - Ranging Delegate
    
    func beaconManager(_ manager: Any, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
        
        
    }
    
    func beaconManager(_ manager: Any, didExitRegion region: CLBeaconRegion) {
        
        DispatchQueue.main.async {
            if region == self.meetingRoomRegion{
                //  DispatchQueue.once(token: "maindoor", block: { (any) in
                
                let identifier = "UYLLocalNotification"
                let content = UNMutableNotificationContent()
                content.title = "Sonin"
                content.sound = UNNotificationSound.default()
                content.setValue(true, forKeyPath: "shouldAlwaysAlertWhileAppIsForeground")
                let trigger = UNTimeIntervalNotificationTrigger(
                    timeInterval: 1.0,
                    repeats: false)
                
                content.body = "Hi \(self.appDelegate.clientEmail), thanks for visiting us. Looking forward to our next meeting"
                let request = UNNotificationRequest(identifier: identifier,
                                                    content: content, trigger: trigger)
                self.center.add(request, withCompletionHandler: { (error) in
                    if error != nil {
                        print("Sonething wrong")
                    }
                })
                // })
            }
        }
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        completionHandler(UNNotificationPresentationOptions.alert)
    }
    
}

public extension DispatchQueue {
    
    private static var _onceTracker = [String]()
    
    /**
     Executes a block of code, associated with a unique token, only once.  The code is thread safe and will
     only execute the code once even in the presence of multithreaded calls.
     
     - parameter token: A unique reverse DNS style name such as com.vectorform.<name> or a GUID
     - parameter block: Block to execute once
     */
    public class func once(token: String, block:(Void)->Void) {
        objc_sync_enter(self); defer { objc_sync_exit(self) }
        
        if _onceTracker.contains(token) {
            return
        }
        
        _onceTracker.append(token)
        block()
    }
}

