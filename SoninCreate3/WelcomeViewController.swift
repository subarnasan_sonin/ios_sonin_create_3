//
//  ViewController.swift
//  SoninCreate3
//
//  Created by Sonin on 27/04/2017.
//  Copyright © 2017 Sonin. All rights reserved.
//

import UIKit

class WelcomeViewController: BaseVC {
    
    
   @IBOutlet weak var nameTF: UITextField!

    @IBOutlet weak var sendButton: UIButton!
    
    @IBOutlet weak var currentScrollView: UIScrollView!
    //@IBOutlet weak var nameContainerView: UIView!
    
    
        //MARK: - Life Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        
        let userId = Network.sharedInstance.getLoggedUserId()
        
        if (!userId.isEmpty) {
            
            self.showToMainView()
        }
        
        self.setupView()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.addObserverForKeyboardNotification()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
        
        self.removeObserverForKeyboardNotification()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK: - INITIAL SETUP 
    
    func setupView() {
        
        self.activityOverlay.createOverlay(onTopOf: self.view)
        self.targetScrollView = self.currentScrollView
        
        self.nameTF.delegate = self
        
    }
        
    func showToMainView() {
        
        if let viewController = sb?.instantiateViewController(withIdentifier: "ViewController") as? ViewController {
            
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    //MAKR: - send client details for the first time
    
    func sendName() {
        
        if let emailString : String = self.nameTF.text, !emailString.isEmpty, let uniqueId : String =  UIDevice.current.identifierForVendor?.uuidString {
            
            
            self.activityOverlay.showOverlay()
            
            WebServices.sharedInstance.sendClientDetails(userEmail: emailString, uniqueId: uniqueId, completion: { [weak self] (success, dict, message) in
                
                if let strongSelf = self {
                    
                    strongSelf.activityOverlay.hideOverlay()
                    
                    if (success) {
                        
                        strongSelf.showToMainView()
                    }
                }
                
            })
            
        }
        
    }
    
    
    
    
    //MARK: - IBACTIONS
    
    @IBAction func sendAction(_ sender: Any) {
                
        self.sendName()
    }
    

}

